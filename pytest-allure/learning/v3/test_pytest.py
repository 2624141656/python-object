import pytest, requests
# 提取参数的main
import jsonpath
# 引入allure
import allure
import os

from xToolkit import xfile  # 读取对应excel
# from hctest_excel_to.excel_to import Excel  # 研究一下别人怎么封装的
from string import Template
from learning.v3.global_value import GlobalVar

"""

确定当前这个接口调用时，是否需要用到以前接口的参数
确定当前接口返回结果时，是否需要提取参数出来
在excel或者sql中告诉程序是不是需要提取参数-是否需要参数
解决问题：
    怎么取excel中的数据？------xToolkit的xfile
    怎么让一个方法遍历所有要访问的接口？--------pytest的@pytest.mark.parametrize
    怎么让一个requests通用？-------requests的request
    要取出的对应的参数无法确认格式和类型怎么办？------eval
    提取的参数存哪里去？------存字典
    怎么知道这个url是正常url还是需要加内容的url？-------Template的${}
    提取的值，不知道在json中的哪一层怎么办？--------JsonPath的$..object

"""

# 1、读取excel，并且把读出来的数据转换成列表
测试用例列表 = xfile.read("接口测试用例.xls").excel_to_dict(sheet=1)
print(测试用例列表)  # 怎么驱动 test_case_exec 执行？


# --- pytest有一个对应的方式 参数化机制 --- 自动循环 DDT
# @pytest.mark.parametrize("case_info") pytest装饰器


# for循环？什么时候终止怎么知道？
# eval 函数，会自动按你的数据格式，格式化掉对应的数据()[]
# parametrize规则：如果传入一个列表作为参数，根据列表长度来循环取值进行执行
# 密钥 -- 变量
@pytest.mark.parametrize("case_info", 测试用例列表)
# 引用一个pytest装饰器，我要mark的parametrize。
# 告知，函数的【case_info】这个参数的数据由装饰器规定，来至于【测试用例列表】
# 则【测试用例列表】把值赋予【case_info】，【case_info】再通过装饰器将列表传给test_case_exec方法
def test_case_exec(case_info):
    # 如果url里有变量参数{}怎么办？取出来、替换掉
    # 先确定是否需要获取之前接口的参数
    url = case_info["接口URL"]
    # 实例化GlobalVar，然后访问show_dict方法，获取所有字典
    dic = GlobalVar().show_dict()
    # 判断是否含有$
    if "$" in url:
        # 如果有，则从字典中查询并替换。换谁？---定义一个对象做全局变量
        url = Template(url).substitute(dic)

    # 以上所有请求都在为了组装一个请求，这个请求中会有许多东西

    rep = requests.request(
        # url=case_info["接口URL"], 替换为从字典去查
        url=url,
        method=case_info["请求方式"],
        # eval ：根据数据类型格式化为对应的类型
        params=eval(case_info["URL参数"]),
        data=eval(case_info["JSON参数"])
    )

    # 然后将数据写入到核心对象中去
    # 判断对应的提取参数是否是空的，如果不是空的，则提取参数
    if case_info['提取参数'] is not None or case_info['提取参数'] != '':
        # 提取要保存的参数，比如token
        lst = jsonpath.jsonpath(rep.json(), '$..' + case_info['提取参数'])
        # 保存进字典
        GlobalVar().set_dict(case_info['提取参数'], lst[0])

    assert rep.status_code == case_info["预期状态码"]

    """
    所有自动化的最终目标：达成无人值守测试
    打破常规测试的时间浪费死循环
    所有的测试执行过程（让计算机完成） 不应再让人执行，因为漏测率高
    测试执行 --发版 --回退 --部署 --监控 --预警
    
    框架已经解决了测试执行的问题:
    接口测试还要进行：数据校验
    """

    """
    数据校验的问题 -- assert 断言 -- 给计算机一个预期值，计算机去自己比对是不是一致，是就返回true，不一致就抛异常
    使用方式：assert 【条件】
    断言结果 是否是正确的？
    接口测试只检验状态码？？
        解决的问题：
        不同的接口有不同要验证的数据？
        GetFileAttributesEx() "D:	est-html\html" failed (123: The filename, directory name, or v
    """

    assert rep.status_code == 200
    # assert rs.json()[case_info["需要断言的字段"]] == case_info["断言的预期值"]

    """
    解决问题：
        在返回结果的更深层怎么办？-----jsonpath
    """
    # 实际返回值 = jsonpath.jsonpath(rs.json(), "$.." + case_info["需要断言的字段"])[0]
    # assert 实际返回值 == case_info["断言的预期值"]

    """
    解决复杂问题：
        断言如果不是一个字段？
        断言，如果是判定它大于 小于 包含 等于 大于等于 小于等于 不等于 ？
        行为方式是一样的
    """

    """
    无人值守的第二要素：要把测试结果告知别人 --- 测试报告 --- allure
    allure（纯命令行运行的，意味着我们不需要去改变代码）
    allure-pytest,os
    引入后，只需要在main函数中写入命令
    
    解决执行问题 --- 能不能把这段代码，让计算机自己在合适时机运行
    合适的实际？ --- 开发写完了再测 --- 提交代码之后
    运行python代码 --- python代码名
    Jenkins：可以执行命令，可以建立触发器
    
    Gogs == git相关
    """


if __name__ == "__main__":
    # pytest.main(['-vs'])  # pytest 的启动命令
    # -s,-v ===== pytest的启动命令
    # --capture=sys ===== 使用系统方式执行
    # test_pytest.py ===== 执行的当前的脚本名字是什么
    # --clean-alluredir ===== 在执行的过程中,清理原本生成的allure报告
    # --alluredir=allure-results ===== 生成的测试数据要放到什么地方去
    pytest.main(['-s', '-v', '--capture=sys', 'test_pytest.py', '--clean-alluredir', '--alluredir=allure-results'])
    # pytest.main(['-vs', '--capture=sys', 'String里的Template.py', '--clean-alluredir', '--alluredir=allure-results'])
    # 通过命令告知存储在哪个位置
    os.system(r"allure generate -c -o 测试报告")
