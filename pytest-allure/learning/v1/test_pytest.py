import pytest, requests

from xToolkit import xfile  # 读    取对应excel
# from hctest_excel_to.excel_to import Excel  # 研究一下别人怎么封装的

# 1、读取excel，并且把读出来的数据转换成列表
测试用例列表 = xfile.read("接口测试用例.xls").excel_to_dict(sheet=1)
print(测试用例列表)  # 怎么驱动 test_case_exec 执行？


# --- pytest有一个对应的方式 参数化机制 --- 自动循环 DDT
# @pytest.mark.parametrize("case_info") pytest装饰器


# for循环？什么时候终止怎么知道？
# eval 函数，会自动按你的数据格式，格式化掉对应的数据()[]
@pytest.mark.parametrize("case_info", 测试用例列表)
# 引用一个pytest装饰器，我要mark的parametrize。
# 告知，函数的【case_info】这个参数的数据由装饰器规定，来至于【测试用例列表】
# 则【测试用例列表】把值赋予【case_info】，【case_info】再通过装饰器将列表传给test_case_exec方法
def test_case_exec(case_info):
    rep = requests.request(
        url=case_info["接口URL"],
        method=case_info["请求方式"],
        params=eval(case_info["URL参数"]),
        data=eval(case_info["JSON参数"])
    )

    assert rep.status_code == case_info["预期状态码"]


if __name__ == "__main__":
    pytest.main(['-vs', '--capture=sys']) # pytest 的启动命令
