import requests
import jsonpath

url = "http://shop-xo.hctestedu.com/index.php?s=api/user/login"
headers = {
    "application": "app",
    "application_client_type": "weixin",
}
params = {
    "application": "app",
    "application_client_type": "weixin",
}
data = {
    "accounts": "huace_xm",
    "pwd": 123456,
    "type": "username"
}
getRes = requests.post(url=url,
                       headers=headers,
                       params=params,
                       data=data
                       )
# 关联方法：前一个方法采集的token，在后面的方法要用上。
# 设置一个变量解决参数传递问题
res_json = getRes.json()
print(res_json)

# 获取一个json数组对象中对应的参数取出
data1 = res_json["data"]
token1 = res_json["data"]["token"]
print(token1)
# 上述方式无法确认层级或者层级贴别多的时候怎么获取？jsonpath----$..token---..表示不论上方多少层级
token = jsonpath.jsonpath(res_json, '$..token')[0]
print(token)

# 接口关联 对应的token通过传参 方式。在上方由 jsonpath获取
url2 = "http://shop-xo.hctestedu.com/index.php?s=api/cart/save&token="+token
postRes = requests.post(url=url2, headers=headers, params=params, data=data)
print(postRes.json())
