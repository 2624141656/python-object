# 定义一个对象类
class GlobalVar(object):
    # 定义一个类空字典
    _global_dict = {}  # 最大程度保证这个字典不被污染

    def set_dict(self, key, value):
        self._global_dict[key] = value

    def get_dict(self, key):
        return self._global_dict[key]

    # 获取所有字典
    def show_dict(self):
        return self._global_dict
