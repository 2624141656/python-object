import requests


# 核心执行器
def send_request(url, method, params, data):
    requests.request(url=url,
                     method=method,
                     params=params,
                     data=data)

# 依旧要每执行一个接口调用一次。怎么实现用一个list解决？
# -- 使用excel驱动
# 1、读取excel
# 2、把excel的数据变成列表
# 3、循环调用 函数
# 有什么隐患？？有可能导致流程无法启动或中断
# 4、如果中间有一条用例错误？怎么解决因为一个用例错误导致停止
# ----解决停止问题 pytest（有用例错了不会停止，只会报错）
# 5、pytest 脚手架



