# String 里的 Template
"""

如果碰到特殊符号 ${}
它会自动用字典的key去替换掉 ${} 同名的变量
python自带，无需安装

"""
import requests
from string import Template

data1 = {"age": 11}
message = "你好，我今年${age}岁了"
print(Template(message).substitute(data1))

data2 = {
    "age": 15,
    "str": "Test_Template_in_String"
}
url = "https://www.baidu.com/s?wd=${str}"
print(Template(url).substitute(data2))


