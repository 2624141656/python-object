"""
import -- 简单的包 -math 类偏少，有的甚至只有一个类
from  -- 复杂的包 -- 多个类 -- 每个类都有自己的方法
webdriver 提供一个打开浏览器的方法
让python打开浏览器需要驱动
"""

import time
from selenium import webdriver
# executable_path已启用,改用service方式，因此引包
from selenium.webdriver.chrome.service import Service
# 新版driver.find_element_by_id方法不存在(暂时好像可用)，改用支持的方式，需引包
from selenium.webdriver.common.by import By

"""完整的webUI 搜索功能测试流程"""

"""八大元素定位：
    ID = "id" --- 标签内的唯一键，但可以不写，因此并非每个标签都有id
    XPATH = "xpath" --- 位置定位，路径定位。鼠标右键tag-->copy-->copy XPATH：【//*[@id="hotsearch-refresh-btn"]/span】元素没有id没有name没有class或者属性相同或者刷新后属性会变的元素，可以用XPATH定位
    LINK_TEXT = "link text" --- 针对文本链接的。可以不用管标签，直接通过文本就能定位a标签的链接
    PARTIAL_LINK_TEXT = "partial link text" --- 模糊定位。有时候一个链接文本很长，为了美观和方便，截取输入
    NAME = "name" --- 标签内的唯一键，但可以不写，因此并非每个标签都有name
    TAG_NAME = "tag name" --- 一个尖括号<>，即为一个tag.即：html的每个元素都是tag，一个tag往往定义一类功能，很难通过一个tag去区分一个元素
    CLASS_NAME = "class name"
    CSS_SELECTOR = "css selector

"""

# 0、加载驱动
driver_path = r"D:\install\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
# 1、打开浏览器
# 旧：driver = webdriver.Chrome(executable_path=driver_path) executable_path已启用
driver = webdriver.Chrome(service=driver_path_service)
# 2、输入网址
driver.get("https://www.baidu.com/")
# 运行发现【WebDriverException: Message: unknown error: cannot find Chrome binary】【version】少驱动
# 3、找到元素，找到的元素是什么元素？debug试一下==selenium将web元素改为自己内部的实例对象
# search_input = driver.find_element_by_id("kw")
search_input = driver.find_element(By.ID, "kw")

print(search_input.tag_name)
print(search_input.get_attribute("type"))
print(search_input.parent)

# 4、输入关键字
search_input.send_keys("测试开发学习资源By.ID")
time.sleep(1)
# 5、点击搜索
"""
find_element---By.ID
"""
driver.find_element(By.ID, "su").click()
time.sleep(1)
# 6、验证结果 --- assert 断言
"""断言 判断一个表达式（true/false），出错的时候自定义提示
返回是false --触发异常
特性：条件不满足直接返回错误，不必再继续运行了
"""
# 有时候可能太快了，title都还没出来就断言了，所以加一些sleep time
assert driver.title == "测试开发学习资源By.ID_百度搜索"
# 自定义异常---自定义提示
# assert driver.title != "测试开发学习资源_百度搜索", "断言说明，我们的标题就是【测试开发学习资源_百度搜索】"
print("find_element---By.ID测试通过")
# 因为下面用例还需要输入框输入，因此做清除输入内容操作
search_input.clear()
# 7、关闭浏览器
# driver.quit()

"""
find_element---By.NAME
"""
search_input_name = driver.find_element(By.NAME, "wd")
search_input_name.send_keys("测试开发学习资源By.NAME")
time.sleep(1)
driver.find_element(By.ID, "su").click()
time.sleep(1)
assert driver.title == "测试开发学习资源By.NAME_百度搜索"
print("find_element---By.NAME 测试通过")
search_input_name.clear()

"""
find_element---By.CLASS_NAME
"""
search_input_class_name = driver.find_element(By.CLASS_NAME, "s_ipt")
search_input_class_name.send_keys("测试开发学习资源By.CLASS_NAME")
time.sleep(1)
driver.find_element(By.CLASS_NAME, "s_btn").click()
time.sleep(1)
assert driver.title == "测试开发学习资源By.CLASS_NAME_百度搜索"
print("find_element---By.CLASS_NAME 测试通过")
search_input_class_name.clear()

"""
find_element---By.TAG_NAME
"""
# search_input_tag_name = driver.find_element(By.TAG_NAME, "input")
# search_input_tag_name.send_keys("测试开发学习资源By.TAG_NAME")
# time.sleep(1)
# 如果一个页面只有一个相同tag尚且能用，这里也是input，则会报错。因此以tag定位元素很少使用
# driver.find_element(By.TAG_NAME, "input").click()
# driver.find_element(By.ID, "su").click()
# time.sleep(1)
# assert driver.title == "测试开发学习资源By.TAG_NAME_百度搜索"
# search_input_tag_name.clear()
# print("find_element---By.TAG_NAME 测试通过")

"""
find_element---By.LINK_TEXT
"""
search_input_link_text = driver.find_element(By.LINK_TEXT, "登录")
# search_input_link_text.click()
driver.execute_script("arguments[0].click();", search_input_link_text)
time.sleep(2)
tang_body = driver.find_element(By.ID, "passport-login-pop")
# 判断弹框内容不为空，证明用例通过
assert tang_body.text
# 因为下方用例还需要开启弹框，因此这里做关闭弹框操作
close_btn = driver.find_element(By.ID, "TANGRAM__PSP_4__closeBtn")
driver.execute_script("arguments[0].click();", close_btn)
print("find_element---By.LINK_TEXT 测试通过")
time.sleep(1)

"""
find_element---By.PARTIAL_LINK_TEXT
如果能匹配多个元素会怎么样？---会访问第一个，因此要保证模糊搜索唯一性
"""
search_input_partial_link_text = driver.find_element(By.PARTIAL_LINK_TEXT, "录")
driver.execute_script("arguments[0].click();", search_input_link_text)
time.sleep(2)
tang_body_partial_link = driver.find_element(By.ID, "passport-login-pop")
assert tang_body_partial_link.text
driver.execute_script("arguments[0].click();", close_btn)
print("find_element---By.PARTIAL_LINK_TEXT 测试通过")
time.sleep(1)

"""
find_element---By.XPATH
xpath---原本是用于xml文档中查找信息的语言
"""
# xpath_span = "//*[@id='hotsearch-refresh-btn']/span"
# * 通配符 ，
# @ 找对应属性，
# / 从根目录开始 ，
# //book 不管从哪个地方开始而找出所有book ，
# [n] 第n个或倒数第n个[last()-n]
xpath_span = "//*[@id='s-top-loginbtn']"
search_input_xpath = driver.find_element(By.XPATH, xpath_span)
driver.execute_script("arguments[0].click();", search_input_xpath)
time.sleep(2)
tang_body_xpath = driver.find_element(By.ID, "passport-login-pop")
assert tang_body_xpath.text
driver.execute_script("arguments[0].click();", close_btn)
print("find_element---By.XPATH 测试通过")
time.sleep(1)

"""
find_element---By.CSS_SELECTOR
与xpath类似，比xpath简介。学习成本偏高
css层级关系定位。各种符号有特殊代表：
# id
. class
> 子元素层级

find_element(By.CSS_SELECTOR, "id属性值")
-->find_element(By.CSS_SELECTOR, "#id")
-->find_element(By.CSS_SELECTOR, ".s_int")
-->find_element(By.CSS_SELECTOR, "[style='display']")
-->find_element(By.CSS_SELECTOR, "#kw>div>#ko>[style='display']")
... ...
"""
search_input_css_selector = driver.find_element(By.CSS_SELECTOR, "#kw")
search_input_css_selector.send_keys("测试开发学习资源By.CSS_SELECTOR")
time.sleep(2)

# 关闭浏览器
driver.quit()
