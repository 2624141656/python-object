""""""

"""
"with 代码 as 变量" 
    这里的with执行语句如果不出错，就会赋值给as后面的变量
    用于文件读取，with块完毕后，自动关闭文件

"""
# 打开一个文件，"r"表示要去读，
with open(r"E:\日常记录\测试.txt", "r", encoding="utf-8") as f:
    print(f.read())

# 异常情况
with open(r"E:\日常记录\测试1.txt", "r", encoding="utf-8") as f:
    print(f.read())
