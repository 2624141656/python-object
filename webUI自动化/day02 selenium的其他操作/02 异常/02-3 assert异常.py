""""""

"""
断言机制 -- 关键字assert后面的表达式一旦为false就直接抛异常
异常特性：中断程序，出现红字
assert独特特性：assert关键字后面可以加异常提示
"""

s = "hello python"
assert s[1] == "o", "字符串的第二个元素{} 不等于 字符串o".format(s[1])
