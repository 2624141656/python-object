""""""

"""
异常类型：
    1、红字（中断程序，控制台打印红字）
    2、try...except 捕获异常，执行except代码块内容
    3、try...finally 
    
    try:
        代码块（无论是否会异常）
    finally:
        代码块（无论异常是否发生，finally代码块一定执行）
        
    两种特殊异常方式也可以联合使用
"""

s = "hello python"
try:
    print("=========您好Exception1=========")
    print(s[100])
    print("上面已异常，此处无法处理到")
except IndexError:
    # 异常处理方案
    print("只要出异常，这里就会执行")
finally:
    print("不管有没有异常，这里都会执行")

try:
    print("=========您好Exception2=========")
    print(s[1])
    print("上面无异常，此处可以处理到")
except IndexError:
    # 异常处理方案
    print("只要出异常，这里就会执行")
finally:
    print("不管有没有异常，这里都会执行")

"""
finally 应用场景，无论如何关闭浏览器等。避免各种情况下网页越开越多。或关闭文件，关闭数据库链接等。
"""
# finally:
#     driver.quit()
#     print("不管有没有异常，这里都会执行")

try:
    print("=========您好Exception3=========")
    print(s[100])
    print("上面已异常，此处无法处理到")
finally:
    print("不管有没有异常，这里都会执行")
