""""""

"""
异常处理方案：
    try:
        代码块（我们自己觉得可能这里会出错）
    except 描述异常类型:
        代码块（出现异常了，执行这里）不让error打印影响阅读   
"""

s = "hello python"
try:
    print("您好IndexError")
    print(s[100])
    # 依旧已经中断代码
    print("上面已异常，此处无法处理到")
except IndexError:
    # 异常处理方案
    print("不出现红字error，只抛出此处文字")

try:
    print("您好Exception")
    print(s[100])
    print("上面已异常，此处无法处理到")
except Exception:
    # 异常处理方案
    print("不出现红字error，只抛出此处文字，无法确定异常类型，直接使用Exception")
