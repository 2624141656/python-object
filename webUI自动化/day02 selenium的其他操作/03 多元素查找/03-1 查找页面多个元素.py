""""""
"""
1、功能测试用例 -- 移交自动化
2、自动化组 对用例进行筛选 -- 评审。人工更快的、无法自动化实现的(性价比低)，不进行自动化
3、编写脚本
4、转换成自动化用例
5、执行并输出测试报告

find_elements_by_xx 获取同样条件下的一组元素
新版：find_elements(By.ID,"同样的元素id或者name或者其他")
返回值是一个列表

"""
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 关闭页签快捷键
from selenium.webdriver import Keys
from selenium.webdriver import ActionChains

# 0、加载驱动
driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
# options = Options()
# options.add_argument("--headless=new")
# options.add_experimental_option("excludeSwitches", ["enable-logging"])

option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
# 1、打开浏览器 创建chrom的实例对象
driver = webdriver.Chrome(service=driver_path_service, options=option)
# 2、页面最大化，输入网址。包括后期要使用ActionChains也需要页面最大化，否则快捷键操作可能会失败
driver.maximize_window()
driver.get("https://www.baidu.com/")
driver.implicitly_wait(30)
search_input = driver.find_element(By.ID, "kw")
# 4、输入关键字
search_input.send_keys("测试开发学习资源")
WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//*[@id="u"]/a[1]')),
                               "错误示例：输入框内文字还未输入完成")
# 5、点击搜索
driver.find_element(By.ID, "su").click()
sleep(1)
# 6、验证结果 --- assert 断言
print(driver.title)
assert driver.title == "测试开发学习资源_百度搜索", "标题不对应"
print("搜索【测试开发学习资源】成功")
# driver.close()

# 跳转百度热搜 --- 打开新的页签（多窗口）
try:
    print("跳转百度热搜 --- 打开新的页签")
    # 打开新的页签
    newWindow = 'window.open("https://www.baidu.com/")'
    # 打开方式
    driver.execute_script(newWindow)
    # 移动句柄，对打开的新的页面进行操作
    driver.switch_to.window(driver.window_handles[1])
    print("当前页签的url为：{}".format(driver.current_url))

    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.ID, "lg")),
        "等待跳转到百度首页")
    a_link = driver.find_element(By.XPATH, '//*[@id="s-hotsearch-wrapper"]/div/a[1]/div/i[1]')
    a_link.click()

    # 移动句柄，对打开的新的页面进行操作
    driver.switch_to.window(driver.window_handles[2])
    # driver.execute_script()
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="sanRoot"]/header/div[2]')),
        "等待跳转到热搜首页")

    print(driver.title)
    assert driver.title == "百度热搜"
    print("跳转百度热搜页面测试通过")
    # 访问测试百度热搜的一个列表
    # 1、取到要测的整个list -- // 底下的所有a标签
    hot_lst = driver.find_elements(By.XPATH, '//*[@id="sanRoot"]/main/div[1]/div[1]/div[2]/a')
    print("热搜有{}条".format(len(hot_lst)))
    # 2、循环遍历hot_lst
    # 因为页面只展示len(hot_lst) - 1条，如果不减一，最后一条点不到，会报错。rang函数是左闭右开[)，所以正常情况下不用-1
    for i in range(len(hot_lst) - 1):
    # for i, hots in enumerate(hot_lst):
        print(i)
        # 页面在来回切换的过程中，find_elements-By.XPATH会被认为不安全，hot_lst及其他所有元素被认定过期。所以全新获取一次。但是此处是页签的转换因此不需要
        # hot_lst = driver.find_elements(By.XPATH, '//*[@id="sanRoot"]/main/div[1]/div[1]/div[2]/a')
        print(hot_lst[i])
        # print(hots)
        hot_lst[i].click()
        # hots.click()
        # 获取全部页面句柄
        all_handles = driver.window_handles
        # 移动句柄，对打开的新的页面进行操作
        driver.switch_to.window(all_handles[3])
        WebDriverWait(driver, 5).until(EC.number_of_windows_to_be(4), "等到切换到最新的页签")
        assert driver.title != "百度热搜"
        print("跳转热搜新闻【{}】详情测试通过".format(driver.title))
        sleep(2)
        window_handle = driver.current_window_handle
        # 将当前句柄定位到新打开的页面
        # driver.switch_to.window(all_handles[-1])
        driver.switch_to.window(window_handle)
        # 关闭当前标签页（最后一页）？？？---no such window: target window already closed：driver 添加 options
        driver.close()
        # 关闭新打开的标签：快捷键 ctrl+w
        # ActionChains(driver).key_down(Keys.CONTROL).send_keys("w").key_up(Keys.CONTROL).perform()
        # action = ActionChains(driver)
        # action.send_keys(Keys.CONTROL, 'w')
        # action.key_down(Keys.CONTROL).send_keys('w').key_up(Keys.CONTROL).perform()
        # 跳转源点击页的页签，才能继续操作循环点击
        driver.switch_to.window(all_handles[2])
        WebDriverWait(driver, 5).until(EC.number_of_windows_to_be(3), "等到切换到最新的页签")
# 学习使用，正常场景中，详情页测试覆盖了对应标签的测试
except Exception as e:
    print("打开新的页签 出错了，错误是：{}".format(e))
finally:
    # driver.close()
    print("打开新的页签 顺序输出，下方用例仍要访问，暂不退出浏览器")
    # driver.quit()

# 原页签跳转页面
try:
    print("原页签跳转页面")
    driver.get("http://novel.hctestedu.com/")
    driver.implicitly_wait(30)

    book_lst = driver.find_elements(By.XPATH, '//*[@id="topBooks1"]/dd/a')
    for i in range(len(book_lst)):
        # ABA问题：页面在来回切换的过程中，find_elements-By.XPATH会被认为不安全，hot_lst及其他所有元素被认定过期。所以全新获取一次。
        book_lst = driver.find_elements(By.XPATH, '//*[@id="topBooks1"]/dd/a')

        book_lst[i].click()
        assert driver.title != "读书屋_原创小说网站"
        print("跳转小说【{}】详情页测试通过".format(driver.title))
        # 回退原有页面
        driver.back()
        # 学习使用，正常场景不会拿这个做自动化
except Exception as e:
    print("原页签跳转页面 出错了，错误是：{}".format(e))
finally:
    # driver.quit()
    print("原页签跳转页面 顺序输出，下方用例仍要访问，暂不退出浏览器")
    # driver.close()

# 循环测试批量的列表
# try:
#     print("try2")
#     link_dt = driver.find_elements(By.XPATH, '//*[@id="1"]/div/div/div[2]/div[1]')
#     print("热搜总数量".format(len(link_dt)))
#     print(driver.title)
#     assert driver.title != ""
# except Exception as e:
#     print("出错了，错误是：{}".format(e))
#
#
# finally:
#     driver.quit()
sleep(5)
driver.quit()
