""""""
"""
等待机制：都是停止当前代码的继续运行

显性等待更加灵活
WebDriverWait类 显性等待类
参数：
    driver: webDriver 实例
    timeout: 超时时间
    poll_frequency: 间隔时间，默认是0.5 -- 控制多久操作一次WebDriverWait.until.method
    ignored_exceptions: 忽略异常(元组数组) -- 一旦发生了我们定义的异常，则不中断代码继续等待。
                        如果不是定义的，则照旧中断
"""

"""
WebDriverWait.until 方法
    method: 在等待期间内，每隔一段时间调用当前的方法，如果不能执行则返回false，否则返回值为正常值（poll_frequency控制）
    message: 如果超时，TimeoutException，把message传入异常
WebDriverWait.until_not 方法 当某条件不成立则继续执行
"""

from selenium.webdriver.support.wait import WebDriverWait
# __init__ 是什么方法？--构造方法。
# 争对于某个对象类，有对应的属性，构造方法就是当我们每个对象要在外部被人使用，则要先实例化，
# 在内存地址中copy一个副本对象 实例，
# 在创造副本的过程中，因为有不同的属性存在，在实例化之前，属性无值，实例化后，就可以向对应的属性赋值--构造方法
# 每个对象都默认有一个这样的构造方法，当对象实例化WebDriverWait()的时候，赋值给变量的时候，这个变量获取到的就是里面的对象
# 详见 面向对象 课程视频

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

# 0、加载驱动
driver_path = r"D:\install\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
# 1、打开浏览器
driver = webdriver.Chrome(service=driver_path_service)
# 2、输入网址
driver.get("https://www.baidu.com/")
# 3、找到元素
search_input = driver.find_element(By.ID, "kw")
# 4、输入关键字
search_input.send_keys("测试开发学习 WebDriverWait")
# 5、点击搜索
driver.find_element(By.ID, "su").click()
# 识别页面是否跳转成功 --- 跳转后独有的东西不为空，则成功
locator = (By.XPATH, '//*[@id="u"]/a[1]')
# time.sleep(3)
# 显性等待 且 进行比对
# until中不能直接写元素，而是写一个可以执行的方法。因此使用EC的方法调用。
# .click()每隔0.5秒来调用一次EC.presence_of_element_located(locator)
WebDriverWait(driver, 3).until(EC.presence_of_element_located(locator), "没获取到对应元素")
print("当前页面【百度首页】元素已经被获取到")

# 识别页面是否跳转成功 --- 跳转后独有的东西不为空，则成功
locator_error = (By.XPATH, '//*[@id="u"]/div[1]/a[1]')
WebDriverWait(driver, 5).until(EC.presence_of_element_located(locator_error), "错误示例：没获取到对应元素")
print("错误示例：当前页面【百度首页】元素已经被获取到 不会输出")

driver.quit()
