import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

"""
python是由上往下的执行顺序,因此在还没有加载出来前要做等待
强制等待:
    使用方法:time.sleep(秒数)
    程序遇到time.sleep时停滞运行 一段时间后再继续运行

"""

# 0、加载驱动
driver_path = r"D:\install\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
# 1、打开浏览器
driver = webdriver.Chrome(service=driver_path_service)
# 2、输入网址
driver.get("https://www.baidu.com/")
# 3、找到元素
search_input = driver.find_element(By.ID, "kw")
# 4、输入关键字
search_input.send_keys("测试开发学习资源By.ID")
# 5、点击搜索
driver.find_element(By.ID, "su").click()
time.sleep(3)
# 识别页面是否跳转成功 --- 跳转后独有的东西不为空，则成功
find_element = driver.find_element(By.XPATH, "//*[@id='u']/a[1]")
# 6、验证结果 --- assert 断言
assert find_element is not None, "页面跳转失败"
"""
去掉sleep,为什么有时候能加载出来,有时候报错?因为还没跳转成功,就直接做了判断,所以先前一直使用sleep
"""

# 自定义异常---自定义提示
print("find_element---By.ID测试通过")
# 因为下面用例还需要输入框输入，因此做清除输入内容操作
search_input.clear()

# 关闭浏览器
driver.quit()
