import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
"""
每个页面及每个不同的网络下,加载时间不同,强制等待已不通用
"""

"""
implicitly_wait
隐性等待:
    设置一个最长等待时间,如果在规定时间内,网页加载完成,执行下一步
    否则一直等到时间截止,然后执行下一步 -- 程序会一直等待整个页面加载
"""

# 0、加载驱动
driver_path = r"D:\install\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
# 1、打开浏览器
driver = webdriver.Chrome(service=driver_path_service)
# 2、输入网址
driver.get("https://www.baidu.com/")
# 3、针对driver设置等待时间
# 置入监听器，页面加载过程中会有一个事件，加载完成Thread.block，阻塞线程，如果页面加载完成，会有一个事件推送告知
# 接收到后，阻塞消失
driver.implicitly_wait(30)
# 3、找到元素
search_input = driver.find_element(By.ID, "kw")
# 4、输入关键字
search_input.send_keys("测试开发学习资源By.ID")
# 5、点击搜索
driver.find_element(By.ID, "su").click()
# time.sleep(3)
# 识别页面是否跳转成功 --- 跳转后独有的东西不为空，则成功
find_element = driver.find_element(By.XPATH, "//*[@id='u']/a[1]")
# 6、验证结果 --- assert 断言
assert find_element is not None, "页面跳转失败"
"""
去掉sleep,为什么有时候能加载出来,有时候报错?因为还没跳转成功,就直接做了判断,所以先前一直使用sleep
"""

# 自定义异常---自定义提示
print("find_element---By.ID测试通过")
# 因为下面用例还需要输入框输入，因此做清除输入内容操作
search_input.clear()

# 关闭浏览器
driver.quit()
