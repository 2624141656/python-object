from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)

driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.implicitly_wait(30)
driver.get("http://novel.hctestedu.com/")

# 直接定位元素 --- css 元素定位用到的querySelector
# F12 右键元素 --- copy JS path
# .style.display="none" --- 隐藏图标
# 控制css的隐藏或显示
js_querySelector = 'document.querySelector("body > div.header > div.topMain > div > a > img").style.display="none"'
driver.execute_script(js_querySelector)
sleep(5)

driver.quit()

