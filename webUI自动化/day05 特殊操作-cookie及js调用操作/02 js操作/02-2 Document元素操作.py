
"""
HTML 
当浏览器载入HTML文档，它就会成为Document对象
Document对象 是 HTML文档的根节点
Document对象 使我们可以从脚本中对HTML页面中的所有元素进行访问

Document对象 是Window对象的一部分，可以通过windows.document属性对其访问

"""
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)

driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.implicitly_wait(30)
driver.get("http://novel.hctestedu.com/")

# js元素定位

# # id 元素定位
# js_id = "document.getElementById('searchKey').value = '365'"
# driver.execute_script(js_id)

# # class 元素定位
# js_class = "document.getElementsByClassName('s_int')[0].value='江少'"
# driver.execute_script(js_class)

# # name 元素定位
# js_name = "document.getElementsByName('searchKey')[0].value='365'"
# driver.execute_script(js_name)

# # Tag name 元素定位
# js_tagName = "document.getElementsByTagName('input')[0].value='365'"
# driver.execute_script(js_tagName)

# css 元素定位
js_css = "document.querySelector('#searchKey').value='365'"
driver.execute_script(js_css)

js_click = "document.getElementById('btnSearch').click()"
driver.execute_script(js_click)
sleep(2)

driver.quit()




