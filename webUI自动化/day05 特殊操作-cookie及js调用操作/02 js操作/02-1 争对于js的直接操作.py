from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.get("https://www.baidu.com/")
driver.implicitly_wait(30)

baidu_h = driver.current_window_handle
sleep(2)

# 通过js打开一个window
js = "window.open('http://novel.hctestedu.com/')"
# 执行一句js代码
driver.execute_script(js)
sleep(2)

all_h = driver.window_handles
driver.switch_to.window(all_h[-1])
print(all_h)

driver.execute_script("window.close()")
driver.quit()
