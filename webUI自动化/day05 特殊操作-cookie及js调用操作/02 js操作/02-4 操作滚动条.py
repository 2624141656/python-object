from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)

driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.implicitly_wait(30)
driver.get("http://novel.hctestedu.com/")

# 操作滚动条 --- 操作浏览器窗口相关的，都在window --- 拖到最底层：document.body.scrollHeight
jsl = "window.scrollTo(0,document.body.scrollHeight)"
jsl_拖到一半 = "window.scrollTo(0,document.body.scrollHeight/2)"
# 加载页面，休眠3s
sleep(3)
driver.execute_script(jsl)
sleep(3)
driver.execute_script(jsl_拖到一半)

sleep(3)
driver.quit()