"""
常规情况下，token用于做接口测试
"""

"""
selenium页面查询，光有token不够
什么是Cookie：
http协议 本身是无状态的，即服务器无法判断用户身份。
日常操作中又需要知道是谁该怎么办？：Cookie：一小段文本信息
    Client向Server发起request，Server要记录user状态，就使用response向Client浏览器颁发一个Cookie
    Client浏览器就会存储这个Cookie，当浏览器再请求同一网站时，浏览器把请求的网址连同该Cookie一起提交给Server
    Server检查Cookie以辨认user状态

缓存机制：内存
    一般放在 磁盘(硬盘)中
    计算机读数据 有 转数，即帧，越快读越快
    但常因硬盘读写慢导致问题 --> 内存条 读数据更快（从硬盘中存入内存条，计算机再读内存条）
    mysql就在磁盘中  
    
Cookie机制：
    1、Client request Server
    2、Server response+Cookie Client
    3、Client request+Cookie Server
    4、Server response Client
Cookie属性：
    NAME = VALUE 键值对，可以设置要保存的 Key/Value name唯一
    Expires 过期时间，设置的时候要记得去除掉
    Domain 域名
    Path 在当前哪个路径下生成的
    Secure 设置了则只有SSH连接时才会回传
    
session = 会话，表示一个链接状态
"""
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# 0、加载驱动
driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)

option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.get("http://novel.hctestedu.com/")
driver.implicitly_wait(30)

link = driver.find_element(By.LINK_TEXT, "登录")
link.click()
assert driver.title == "会员登录_读书屋", "未进入到登录页面"

before_cookies = driver.get_cookies()
print("登录前，先取Cookie{}".format(before_cookies))

driver.find_element(By.ID, "txtUName").send_keys("17311110000")
driver.find_element(By.ID, "txtPassword").send_keys("1111")
driver.find_element(By.ID, "btnLogin").click()
sleep(2)
assert driver.title == "读书屋_原创小说网站"
assert driver.find_element(By.XPATH, '//*[@id="headerUserInfo"]/span/a[2]').text == "退出"
print("登录测试通过")

after_cookies = driver.get_cookies()
print("登录后，再取Cookie{}".format(after_cookies))

driver.quit()

"""重新打开一次，这次把cookie带上"""
new_driver = webdriver.Chrome(service=driver_path_service, options=option)
new_driver.maximize_window()
new_driver.get("http://novel.hctestedu.com/")

new_driver.delete_all_cookies()
for cookie in after_cookies:
    # selenium 的 cookies 不支持，expiry过期时间，去掉用于避免一些奇怪的问题
    if "expiry" in cookie.keys():
        cookie.pop("expiry")
    new_driver.add_cookie(cookie)
sleep(3)
new_driver.refresh()  # 刷新时，因为cookie已经放进driver了，所以刷新也等价于重新get，也可以直接为登录状态
new_driver.get("http://novel.hctestedu.com/")  # 重新请求，直接为登录状态
sleep(3)

# 可以继续测试作家专区 充值 等其他功能等

new_driver.quit()
