from webUI自动化.day06POM.PageObjects.Dushuwu_index import DushuwuIndex
from time import sleep
from selenium.webdriver.common.by import By

"""
流程类：会有部分重叠
"""

"""
流程类：
将测试步骤打包到一起
流程：肆意组合页面中的内容 操作
example:
    1、搜索 --> 打开浏览器
    2、输入网址
    3、找到对应的搜索框
    4、输入搜索书名关键字
    5、点击运行

"""


# 要测首页，所以继承首页
class DushuwuFlow(DushuwuIndex):
    def dushuwu_search(self, selector, keyword):
        self.open_dushuwu()
        self.type_send(selector, keyword)
        sleep(3)  # 强制等待
        # TODO 以后如果发生修改，直接在此插入代码
        self.click((By.ID, "btnSearch"))
        sleep(2)
        result = self.get_page_title()
        return result

    def dushuwu_register(self, selector, keyword):
        pass
