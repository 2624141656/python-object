from webUI自动化.day06POM.Base.Browser_driver import BrowserDriver
from webUI自动化.day06POM.Base.Base_page import BasePage
from webUI自动化.day06POM.Flow.DushuwuFlow import DushuwuFlow
from webUI自动化.day06POM.PageObjects.Dushuwu_index import DushuwuIndex

from selenium.webdriver.common.by import By

"""封装的根本目的：将所有重复的代码抽离"""
"""
测试用例 具体页
"""


class TestDushuwuSearch(object):
    def dushuwu_1(self):
        browser = BrowserDriver()
        self.driver = browser.open_browser("Chrome")
        dushuwu_flow = DushuwuFlow(self.driver)
        search_result = dushuwu_flow.dushuwu_search((By.ID, "searchKey"), "江少")
        assert search_result == "全部作品_读书屋"
        print("测试通过")
        self.driver.quit()

    def dushuwu_login(self):
        browser = BrowserDriver()
        self.driver = browser.open_browser("Chrome")
        dushuwu_flow = DushuwuFlow(self.driver)
        # ... ...
        pass
        print("测试通过")
        self.driver.quit()


# TODO ？
"""
问：上下方法实例化的driver是同一个对象吗？不是。。。两个页面上跑
答：不是，实例化两次，是不同对象，所占的虚拟内存地址不同
unitTest + pytest结合 产生的影响
"""
ts1 = TestDushuwuSearch()
ts2 = TestDushuwuSearch()
print(ts1 == ts2)  # False
# 同一个对象实例化两次都不相等，不是同一个

ts1.dushuwu_1()
