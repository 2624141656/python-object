from webUI自动化.day06POM.Base.Base_page import BasePage


class DushuwuLogin(BasePage):
    # 调用父类的所有方法
    def open_dushuwu(self):
        self.get_url("http://novel.hctestedu.com/")

    def type_send(self, selector, text):
        self.text_send(selector, text)

    def click(self, selector):
        # 解耦，独有逻辑解耦更便于维护
        self.click_option(selector)
