""""""
"""
封装读书屋首页 -- 举例
1、打开读书屋
2、搜索关键词 -- 江少
3、获取title -- 判断属性，与预期是否一致
页面类：
封装在当前页面或者这个流程上的基本方法
便于后续复用以及维护
"""

from webUI自动化.day06POM.Base.Base_page import BasePage


# TODO 如果哪天页面发生修改，直接修改Objects
class DushuwuIndex(BasePage):
    # 调用父类的所有方法
    def open_dushuwu(self):
        self.get_url("http://novel.hctestedu.com/")

    def type_send(self, selector, text):
        self.text_send(selector, text)

    def click(self, selector):
        # 解耦，独有逻辑解耦更便于维护
        self.click_option(selector)

# 但是POM思想下，依旧有重复代码存在
# 因此 引入 python 的相关框架（工具） 如 unitTest，pytest等
