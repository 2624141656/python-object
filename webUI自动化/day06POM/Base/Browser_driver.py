""""""

"""
封装浏览器的引擎处理
"""
# 找路径的包
import os
from selenium import webdriver
# 新版
from selenium.webdriver.chrome.service import Service


# 定义一个类
class BrowserDriver(object):
    # 打开浏览器的方法（任何浏览器）
    def open_browser(self, browser):
        # 设定浏览器类型
        if "Chrome" == browser:
            # 当前系统在什么路径，就到当前路径开始往下找，找到Utils
            chrome_driver_path = os.path.abspath("../Utils") + "/chromedriver.exe"
            # 旧版
            # driver = webdriver.Chrome(executable_path=chrome_driver_path)
            # 新版
            option = webdriver.ChromeOptions()
            option.add_experimental_option("detach", True)
            driver_path_service = Service(chrome_driver_path)
            driver = webdriver.Chrome(service=driver_path_service, options=option)
            # 窗口最大化
            driver.maximize_window()
        else:
            fire_driver_path = os.path.dirname(os.path.abspath("../Utils") + "/chromedriver.exe")
            driver = webdriver.Chrome(executable_path=fire_driver_path)

        driver.implicitly_wait(15)
        return driver
