"""

将所有基础操作，如：打开url，关闭浏览器，点击，输入，获取元素
一切不可能发生变化的逻辑进行封装

"""


class BasePage(object):
    # 定义构造方法，传递我们的driver对象
    def __init__(self, driver):
        self.driver = driver

    # 打开url
    def get_url(self, url):
        self.driver.get(url)

    # 关闭浏览器
    def quit_browser(self):
        self.driver.quit()

    # 获取元素 selector=(By.xxx,"") 元组类型数据
    def find_element_to(self, selector):
        # 解包概念 *selector的*表示解出来的包（传进来的）是一个元组类型的数据，解包循环再进行一一对应赋值
        ele = self.driver.find_element(*selector)
        return ele

    def find_elements_to(self, selector):
        ele_lst = self.driver.find_elements(*selector)
        return ele_lst

    # """取到元素后的操作"""
    # 输入字符
    def text_send(self, selector, text):
        ele = self.find_element_to(selector)
        ele.clear()
        ele.send_keys(text)

    # 清空
    def text_clear(self, selector):
        ele = self.find_element_to(selector)
        ele.clear()

    # 点击
    def click_option(self, selector):
        ele = self.find_element_to(selector)
        ele.click()

    # 获取页面title断言
    def get_page_title(self):
        return self.driver.title

    # 获取页面url断言
    def get_page_url(self):
        return self.driver.current_url

    # 切换到新的窗口
    def switch_handle(self):
        handles = self.driver.window_handles
        for newHales in handles:
            if newHales != self.driver.current_url:
                self.driver.switch_to.windows(newHales)

    # 切换到指定窗口
    def switch_sum_handle(self, num):
        handles = self.driver.window_handles
        self.driver.switch_to.windows(handles[num])

