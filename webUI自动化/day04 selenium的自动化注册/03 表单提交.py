""""""
"""
特殊符号去搜索：半安全测试
缓存穿透（非常用东西，不走缓存，就会直接访问数据库，攻击数据库如死锁、崩溃等）
设计支付不自动化测：因为要实现难度大且资源浪费大，且支付宝、微信对此种脚本极敏感，会导致反击操作，如封禁等

要做提交的表单，需要表单提交操作
包含 为空校验、格式校验、长度校验、正则校验等等...
"""

"""
注册的表单提交 
"""

from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 关闭页签快捷键
from selenium.webdriver import Keys
from selenium.webdriver import ActionChains
from PIL import Image
import requests
# 图像识别
import pytesseract
# 使用正则表达式 需要引用re模块
import re
import random
import string


# 获取图片
def get_img(image_path):
    img = Image.open(image_path)
    return img


# 图片灰度处理
def image_grayscale_deal(image):
    # 将获取到的image做灰度转化
    img = image.convert('L')
    # 展示灰度图片
    # img.show()
    return img


# 图片二值化处理 -- 改变像素等
def image_thresholding_method(image):
    threshold = 160
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    image_point = image.point(table, '1')
    # image_point.show()
    return image_point


# 图像识别
def captcha_crack(image):
    image_to_string = pytesseract.image_to_string(image)
    return image_to_string


driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.get("http://novel.hctestedu.com/")
driver.implicitly_wait(30)

num = None
try:
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div')), "等待返回读书屋首页")
    register_link1 = driver.find_element(By.LINK_TEXT, "注册")
    register_link1.click()
    register_img_success = driver.find_element(By.XPATH, '//*[@id="chkd"]')
    # 找到image，直接截图
    register_img_success.screenshot("./picture/register_img_success.png")
    # 对图片进行操作
    register_img_success = get_img("./picture/register_img_success.png")
    deal_img_success = image_grayscale_deal(register_img_success)
    image_new_success = image_thresholding_method(deal_img_success)
    result_success = captcha_crack(image_new_success)
    # 最后结果
    num_success = result_success.split("\n")
    num = num_success[0]
    print("当前根据图像识别,并截取验证码部分，获取的最终结果是：{}".format(num))

except BaseException as be:
    print("出错了：", be)
finally:
    sleep(3)


def clear_all_input():
    if driver.title == "会员注册_读书屋":
        # driver.find_elements(By.XPATH, '//*[@id="form2"]/div/ul/li/@input')
        input_lst = driver.find_elements(By.TAG_NAME, "input")
        for i in range(1, 4):
            input_lst[i].clear()


# 完整的注册流程
# driver.find_element(By.ID, "txtUName").send_keys("")
# driver.find_element(By.ID, "txtPassword").send_keys("")
# driver.find_element(By.ID, "TxtChkCode").send_keys("")
# driver.find_element(By.ID, "TxtChkCode").send_keys(num)
# driver.find_element(By.ID, "btnRegister").click()
# assert "手机号不能为空" in driver.find_element(By.ID, "LabErr").text
# print("手机号不能为空 单独校验 测试通过")
# # driver.find_element(By.ID, "txtUName").clear()  # 清空操作，这样很傻，封装
# clear_all_input()  # ...  # 各种格式都要写一遍。。。自己设计封装


# 手机号校验
def phone_number_assert(phone_num):
    phone_pattern = re.compile(r'^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$')
    if phone_pattern.match(phone_num):
        return True
    else:
        return False


# 测试注册功能
# 自研：提取封装
def assert_all_input(tel, psw, code):
    clear_all_input()
    driver.find_element(By.ID, "txtUName").send_keys(tel)
    driver.find_element(By.ID, "txtPassword").send_keys(psw)
    driver.find_element(By.ID, "TxtChkCode").send_keys(code)
    driver.find_element(By.ID, "btnRegister").click()
    text = ""
    sleep(3)
    if driver.title == "会员注册_读书屋":
        text = driver.find_element(By.ID, "LabErr").text
    tel_in = re.search("^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$", tel)
    if tel is None or tel == "":
        assert "手机号不能为空" in text
        print("手机号不能为空 校验 测试通过")
    elif not tel_in:  # 两种方式
        # elif not phone_number_assert(tel):
        assert "手机号格式不正确" in text
        print("手机号格式不正确 校验 测试通过")
    elif psw is None or psw == "":
        assert "密码不能为空" in text
        print("密码不能为空 校验 测试通过")
    elif code is None or code == "":
        assert "验证码不能为空" in text
        print("验证码不能为空 校验 测试通过")
    elif code != num:
        assert "验证码错误" in text
        print("验证码错误 校验 测试通过")
    elif driver.title == "会员注册_读书屋":
        assert "该手机号已注册" in text
        print("该手机号已注册 校验 测试通过")
    else:
        # 全部正确，可以提交注册，校验是否能注册成功-->自动登录成功
        WebDriverWait(driver, 5).until_not(EC.title_contains("会员注册_读书屋"), "等待跳转")
        assert driver.find_element(By.XPATH, '//*[@id="headerUserInfo"]/span/a[1]').text == tel
        print("校验 能注册成功 且自动登录成功")
        # print("注册失败")
    clear_all_input()


import random


# # 随机生成一组tel_lst_len个手机号码
# def create_phone_num_list(tel_lst_len):
#     all_phone_nums = set()  # 存放生成的电话号码
#     while True:  # 因为set会自动去重，因此死循环生成电话号码，直到等于num个号码停止
#         start = random.choice(
#             ['133', '149', '153', '173', '177', '180', '181', '189', '191', '199'])  # 存放前3位的号段，从中随机取一个
#         end = ''.join(random.sample(string.digits, 8))  # 随机生成后面8位数字
#         all_phone_nums.add(f'{start}{end}')  # 拼接前3位和后8位
#         if len(all_phone_nums) >= tel_lst_len:  # 如果号码个数等于num，则停止
#             return all_phone_nums
#             # break


# 随机生成一个手机号码
def create_phone_num():
    # 存放前3位的号段，从中随机取一个
    start = random.choice(['153', '173', '177', '180', '181', '189'])
    # 随机生成后面8位数字
    end = ''.join(random.sample(string.digits, 8))
    # 拼接前3位和后8位
    phone_nums = f'{start}{end}'
    return phone_nums
    # break


# 开始测试用例：
try:
    print("开始校验注册")
    assert_all_input("", "", "")
    assert_all_input("11ss", "", "")
    # number_assert = phone_number_assert("17311110000")
    assert_all_input("17311110000", "", "")
    assert_all_input("17311110000", "1111", "")
    assert_all_input("17311110000", "1111", 0000)
    assert_all_input("17311110000", "1111", num)
    assert_all_input(create_phone_num(), "qwe123", num)
finally:
    sleep(5)
    driver.quit()
