""""""

"""
OCR（光学文字识别）图像识别，Image
Tesseract是一个OCR库 -- 电脑需下载文件
"""
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 关闭页签快捷键
from selenium.webdriver import Keys
from selenium.webdriver import ActionChains
from PIL import Image
import requests
# 图像识别
import pytesseract


# 获取图片
def get_img(image_path):
    img = Image.open(image_path)
    return img


# 图片灰度处理
def image_grayscale_deal(image):
    # 将获取到的image做灰度转化
    img = image.convert('L')
    # 展示灰度图片
    # img.show()
    return img


# 图片二值化处理 -- 改变像素等
def image_thresholding_method(image):
    threshold = 160
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    image_point = image.point(table, '1')
    # image_point.show()
    return image_point


# 图像识别
def captcha_crack(image):
    image_to_string = pytesseract.image_to_string(image)
    return image_to_string


driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=driver_path_service, options=option)
driver.maximize_window()
driver.get("http://novel.hctestedu.com/")
driver.implicitly_wait(30)

# 获取注册验证码 -- 实际场景中，可以让开发写一个固定码，或注释取消校验
try:
    print("法1、通过资源地址直接拿取 -- 无法使用")
    register_link = driver.find_element(By.LINK_TEXT, "注册")
    register_link.click()
    register_img = driver.find_element(By.XPATH, '//*[@id="chkd"]')
    # 获取图片地址
    img_url = register_img.get_attribute("src")
    print(img_url)
    img_stream = requests.get(img_url, stream=True)
    # print(img_stream.content)
    with open("./picture/register_img1.png", "wb") as f:
        img_png = f.write(img_stream.content)
    print("法1取图成功：%s" % img_png)
    # 对图片进行操作
    # 但是获取的图片有干扰线 -- 对图片进行灰度处理，将颜色改为白色和黑色 -- 写一个方法
    img1 = get_img("./picture/register_img1.png")
    deal_img = image_grayscale_deal(img1)
    # 灰度处理后的图片像素点太尖锐 -- 二值化处理,白的更白，黑的更黑 -- 去掉干扰纹，锐化
    image_new = image_thresholding_method(deal_img)
    # 最后结果
    result = captcha_crack(image_new)
    print("当前根据图像识别,获取的最终结果是：{}".format(result))
    num = result.split("\n")
    print(num)
    # 当前web页面的验证码 跟我们获取的验证码不一致？--我们是通过链接方式取，点击则进行访问，每次访问都会不一样
    # 所以拿到无用
except BaseException as be:
    print("出错了：", be)
finally:
    sleep(3)
    # driver.quit()
    driver.back()

try:
    print("法2、通过截图方式拿取 -- 截图 -- 解决取src路径无法通过验证问题")
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/div')), "等待返回读书屋首页")
    register_link1 = driver.find_element(By.LINK_TEXT, "注册")
    register_link1.click()
    register_img_success = driver.find_element(By.XPATH, '//*[@id="chkd"]')
    # 找到image，直接截图
    register_img_success.screenshot("./picture/register_img_success.png")
    print("%s:注册码截图成功！！！" % register_img_success)
    # 对图片进行操作
    register_img_success = get_img("./picture/register_img_success.png")
    deal_img_success = image_grayscale_deal(register_img_success)
    image_new_success = image_thresholding_method(deal_img_success)
    result_success = captcha_crack(image_new_success)
    # 最后结果
    num_success = result_success.split("\n")
    num = num_success[0]  # 数字  数字+汉字(识别率降低-->百度OCR)+字母
    # 无法使用图像识别单独处理的：拼图类（-->破解js）
    # 验证码的意义：阻止自动化，所以可以忽视
    # 用例评审的时候考虑的维度：时间维度、复杂度、资源
    print("当前根据图像识别,并截取验证码部分，获取的最终结果是：{}".format(num))

except BaseException as be:
    print("出错了：", be)
finally:
    sleep(10)
    driver.quit()
