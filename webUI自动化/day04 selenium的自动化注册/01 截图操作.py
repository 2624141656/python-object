""""""
import base64

"""
1、get_screenshot_as_file + get_screenshot_as_png
2、save_screenshot
3、get_screenshot_as_base64

"""

from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 关闭页签快捷键
from selenium.webdriver import Keys
from selenium.webdriver import ActionChains

# 0、加载驱动
driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
# options = Options()
# options.add_argument("--headless=new")
# options.add_experimental_option("excludeSwitches", ["enable-logging"])

option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
# 1、打开浏览器 创建chrom的实例对象
driver = webdriver.Chrome(service=driver_path_service, options=option)
# 2、页面最大化，输入网址。包括后期要使用ActionChains也需要页面最大化，否则快捷键操作可能会失败
driver.maximize_window()
driver.get("http://novel.hctestedu.com/")
driver.implicitly_wait(30)

# 先尝试截取整个首页
try:
    # 方式1
    picture_file1 = driver.get_screenshot_as_file("./picture/testPage1.png")
    print("%s:方式1截图成功！！！" % picture_file1)
    # 方式2 -- 流截图
    picture_file2 = driver.save_screenshot("./picture/testPage2.png")
    print("%s:方式2截图成功！！！" % picture_file2)
    # 方式3 -- base64截图
    picture_file3_base64 = driver.get_screenshot_as_base64()
    # 得到文件流
    # print(picture_file3_base64)
    decode_img = base64.b64decode(picture_file3_base64)
    # 读文件 -- 写文件
    picture_file3 = open("./picture/testPage3.png", "wb")
    picture_file3.write(decode_img)
    picture_file3.close()
    print("方式3截图成功：%s" % picture_file3.name)
except BaseException as be:
    print("出错了：", be)
finally:
    sleep(5)
    driver.quit()

