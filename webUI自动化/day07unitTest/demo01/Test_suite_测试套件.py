""""""
"""
测试套件：
    他可以任意组合我们在unitTest中的任何测试用例
    让他门按顺序执行
"""

import unittest
from webUI自动化.day07unitTest.demo01.Test05_DDT_跳过测试 import Test05
from webUI自动化.day07unitTest.demo01.Test01_unittest_flow import Test01

# 先定义一个测试套件
suite = unittest.TestSuite()  # 获取对应的测试套件 -->[]
suite.addTest(Test05("test_a"))
suite.addTest(Test05("test_b"))
suite.addTest(Test05("test_c"))
suite.addTest(Test01("test_2"))

# 通过unittest的Runner机制运行
runner = unittest.TextTestRunner()
runner.run(suite)

"""
测试结果：
    .s..
    ----------------------------------------------------------------------
    Ran 4 tests in 0.000s
    
    OK (skipped=1)
    ctest--不会执行到------【因为Test01中，实例化并调用 Test01().ctest() ，因此时此处import Test01直接被实例化调用了。若Test05中也有实例化，则排前面（根据import）】
    Test05--setUp
    a
    Test05--tearDown
    Test05--setUp
    c
    Test05--tearDown
    setUp--前置执行
    test_2--以test开头，会执行到
    tearDown--后置执行

"""

"""
unittest 剩下知识点：断言，自行尝试。
见Test05().test_assert()
"""
