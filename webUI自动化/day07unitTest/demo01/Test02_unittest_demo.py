""""""
"""
写一个selenium测试方法，通过这个方法来去看看常规的测试如何执行
    1、搜索 --> 打开浏览器
    2、输入网址
    3、找到对应的搜索框
    4、输入搜索书名关键字
    5、点击运行
"""
import unittest
import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from time import sleep
from selenium.webdriver.common.by import By
from ddt import ddt


class Test02(unittest.TestCase):
    # class Test01():
    def setUp(self) -> None:
        print("setUp--前置执行--加载驱动，打开浏览器等")
        # 当前系统在什么路径，就到当前路径开始往下找，找到Utils
        chrome_driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
        # 旧版
        # driver = webdriver.Chrome(executable_path=chrome_driver_path)
        # 新版
        option = webdriver.ChromeOptions()
        option.add_experimental_option("detach", True)
        driver_path_service = Service(chrome_driver_path)
        # self.driver 表示在当前实例中生效
        self.driver = webdriver.Chrome(service=driver_path_service, options=option)
        # 窗口最大化
        self.driver.maximize_window()
        self.driver.get("http://novel.hctestedu.com/")

    def tearDown(self) -> None:
        print("tearDown--后置执行--强制休眠、关闭浏览器等")
        sleep(3)
        self.driver.quit()

    def test_case1(self):
        search_input = self.driver.find_element(By.ID, "searchKey")
        search_input.send_keys("江少")
        self.driver.find_element(By.ID, "btnSearch").click()
        print("执行用例test_case1")

    def test_case2(self):
        search_input = self.driver.find_element(By.ID, "searchKey")
        search_input.send_keys("最强")
        self.driver.find_element(By.ID, "btnSearch").click()
        print("执行用例test_case2")


"""
引入知识点：DDT
"""

if __name__ == '__main__':
    unittest.main()
