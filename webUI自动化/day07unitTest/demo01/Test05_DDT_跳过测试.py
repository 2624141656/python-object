import unittest
from selenium.webdriver.common.by import By
import os

"""DDT跳过测试操作"""

"""
跳过测试：通过 skip 装饰器去进行处理
    case 中，对一些不需要运行的用例，或者特定条件下才运行的用例，可以通过skip来实现有条件执行，或者跳过执行，
    一旦采用skip装饰器，那么这些对于指定的用例就不执行操作
"""


class Test05(unittest.TestCase):

    # 切面思维
    def setUp(self) -> None:
        print("Test05--setUp")

    def tearDown(self) -> None:
        print("Test05--tearDown")

    def test_a(self):
        print("a")

    # 跳过执行
    @unittest.skip("不执行的原因：测试@unittest.skip不执行test_b方法")
    def test_b(self):
        # 此方法不执行的代码--编译之后，改变了我们的代码结构。连前置后置都不会执行
        # eg: skip
        print("b")

    def test_c(self):
        print("c")

    def test_d(self):
        print("d")

    @unittest.skipIf(1 < 2, "此用例不执行，原因：测试条件1<2是True，则跳过，不允许执行test_e方法")
    def test_e(self):
        print("e")

    flag1 = False
    flag2 = True

    @unittest.skipIf(flag1, "此用例执行，原因：测试条件flag1是False，则不跳过，执行test_f方法")
    def test_f(self):
        print("f")

    @unittest.skipIf(flag2, "此用例不执行，原因：测试条件flag2是True，则跳过，不允许执行test_g方法")
    def test_g(self):
        print("g")

    @unittest.skip("会报AssertionError，测试使用，暂时跳过")
    def test_assert(self):
        print("test_assert")
        self.assertEqual(1, 2, msg="不一致")


"""
测试结果：
    ============================= test session starts =============================
    collecting ... collected 7 items
    
    Test05_DDT_跳过测试.py::Test05::test_a PASSED                            [ 14%]Test05--setUp
    a
    Test05--tearDown
    
    Test05_DDT_跳过测试.py::Test05::test_b SKIPPED (不执行的原因：测试@unittest.skip不执行test_b方法) [ 28%]
    Skipped: 不执行的原因：测试@unittest.skip不执行test_b方法
    
    Test05_DDT_跳过测试.py::Test05::test_c PASSED                            [ 42%]Test05--setUp
    c
    Test05--tearDown
    
    Test05_DDT_跳过测试.py::Test05::test_d PASSED                            [ 57%]Test05--setUp
    d
    Test05--tearDown
    
    Test05_DDT_跳过测试.py::Test05::test_e SKIPPED (此用例不执行，原因：测试条件1<2是True，则跳过，不允许执行test_e方法) [ 71%]
    Skipped: 此用例不执行，原因：测试条件1<2是True，则跳过，不允许执行test_e方法
    
    Test05_DDT_跳过测试.py::Test05::test_f PASSED                            [ 85%]Test05--setUp
    f
    Test05--tearDown
    
    Test05_DDT_跳过测试.py::Test05::test_g SKIPPED (此用例不执行，原因：测试条件flag2是True，则跳过，不允许执行test_g方法) [100%]
    Skipped: 此用例不执行，原因：测试条件flag2是True，则跳过，不允许执行test_g方法
    
    
    ======================== 4 passed, 3 skipped in 0.12s =========================

"""

if __name__ == '__main__':
    unittest.main()
