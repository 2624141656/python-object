# demo Test suite ：第一个特性，前置后置执行方法
import unittest


class Test01(unittest.TestCase):
    # class Test01():
    def setUp(self) -> None:
        print("setUp--前置执行")

    def tearDown(self) -> None:
        print("tearDown--后置执行")

    # self指代当前对象
    def test_1(self):
        print("test_1--必须以test开头，否则不会执行到")

    def test_2(self):
        print("test_2--以test开头，会执行到")

    def ctest(self):
        print("ctest--不会执行到")

    def test_3(self):
        print("test_3--非test命名的方法怎么办？通过以test开头的方法，调用当前对象中非test命名的方法")
        self.ctest()


# 调用方法的两种方式
# 法1、实例化对象 -- 去 https://pythontutor.com/visualize.html 试验一下吧
# 再去调用方法
Test01().ctest()

# 法2、 右键，run，本质上就是跑main线程（不定义则默认）
# 可以直接定义运行使用的main线程
if __name__ == '__main__':
    # 定义为通过unittest来跑
    unittest.main()

""""""
"""
unittest执行结果
    ============================= test session starts =============================
    collecting ... collected 3 items

    test01.py::Test01::test_1 PASSED                                         [ 33%]setUp--前置执行
    test_1--必须以test开头，否则不会执行到
    tearDown--后置执行

    test01.py::Test01::test_2 PASSED                                         [ 66%]setUp--前置执行
    test_2--以test开头，会执行到
    tearDown--后置执行

    test01.py::Test01::test_3 PASSED                                         [100%]setUp--前置执行
    test_3--非test命名的方法怎么办？通过以test开头的方法，调用当前对象中非test命名的方法
    ctest--不会执行到
    tearDown--后置执行


============================== 3 passed in 0.01s ==============================
"""

"""
去掉 Test01(unittest.TestCase) 中的 unittest.TestCase 即 【Test01()】
也可将main及实例化对象调用方法的都去掉 即 【# Test01().ctest() # if __name__ == '__main__':】
    执行结果：
        ============================= test session starts =============================
        collecting ... collected 3 items

        test01.py::Test01::test_1 PASSED                                         [ 33%]test_1--必须以test开头，否则不会执行到

        test01.py::Test01::test_2 PASSED                                         [ 66%]test_2--以test开头，会执行到

        test01.py::Test01::test_3 PASSED                                         [100%]test_3--非test命名的方法怎么办？通过以test开头的方法，调用当前对象中非test命名的方法
        ctest--不会执行到


        ============================== 3 passed in 0.01s ==============================
"""
""""""
