import unittest
from selenium.webdriver.common.by import By
from ddt import ddt, data, unpack
import os

"""DDT特殊操作"""

"""
Test03中，总不能总是将传值写死@data中？--不智能，不便于维护
---通过方法向data装饰器传递值

"""


# 读取文件
def readFile():
    lst = []
    file_path = os.path.abspath("CaseFile") + "/file.txt"
    file = open(file_path, 'r', encoding='utf-8')
    # file.readline()，则返回【[['1'], ['江'], ['少'], ['', ''], ['最'], ['强'], ['']]】
    # file.readlines()，则返回【[['1江少', '最强'], ['2总', '少爷'], ['3宠', '凑数']]】
    for line in file.readlines():
        lst.append(line.strip("\n").split(','))
        print(lst)
    return lst


@ddt
class Test04(unittest.TestCase):
    # class Test01():
    def setUp(self) -> None:
        print("setUp--前置执行--DDT特殊操作demo")
        pass

    def tearDown(self) -> None:
        print("tearDown--DDT特殊操作demo--后置执行")
        pass

    # 传入读取的文件的数据:读取文件的函数
    # @data中要传入参数，则要加上解包的*号,将[[a,b],[c,d]]解包为[a,b],[c,d]
    @data(*readFile())
    @unpack  # 将[a,b],[c,d]解包为第一次传入a,b ；第二次传入c,d
    def test_case_special(self, txt1, txt2):
        print("执行用例test_case_special")
        print(txt1)
        print(txt2)


"""
测试结果：
    ============================= test session starts =============================
    collecting ... collected 3 items
    
    Test04_DDT_special.py::Test04::test_case_special_1___1江少____最强__ PASSED [ 33%]setUp--前置执行--DDT特殊操作demo
    执行用例test_case_special
    1江少
    最强
    tearDown--DDT特殊操作demo--后置执行
    
    Test04_DDT_special.py::Test04::test_case_special_2___2总____少爷__ PASSED [ 66%]setUp--前置执行--DDT特殊操作demo
    执行用例test_case_special
    2总
    少爷
    tearDown--DDT特殊操作demo--后置执行
    
    Test04_DDT_special.py::Test04::test_case_special_3___3宠____凑数__ PASSED [100%]setUp--前置执行--DDT特殊操作demo
    执行用例test_case_special
    3宠
    凑数
    tearDown--DDT特殊操作demo--后置执行
    
    
    ============================== 3 passed in 0.13s ==============================

"""

if __name__ == '__main__':
    unittest.main()
