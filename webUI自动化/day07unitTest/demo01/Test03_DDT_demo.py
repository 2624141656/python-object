import unittest
import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from time import sleep
from selenium.webdriver.common.by import By
from ddt import ddt, data, unpack

"""
test02中两个方法内容除了参数以外均一摸一样，属于重复代码，想要结合unittest要怎么进行封装呢？
多组数据，测试同一组件：操作流程固定，只有数据不同。DDT--数据驱动
引入知识点：DDT
    Data Driven Testing，数据驱动：测试数据的参数化
    python中DDT以装饰器的形式，结合unittest使用，用来装饰测试类
    安装方式：pip install ddt
    包含组件：
        @ddt.ddt
        @ddt.data
        @ddt.unpack

"""


@ddt
class Test03(unittest.TestCase):
    # class Test01():
    def setUp(self) -> None:
        print("setUp--前置执行--加载驱动，打开浏览器等--DDT常规操作demo")
        chrome_driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
        option = webdriver.ChromeOptions()
        option.add_experimental_option("detach", True)
        driver_path_service = Service(chrome_driver_path)
        # self.driver 表示在当前实例中生效
        self.driver = webdriver.Chrome(service=driver_path_service, options=option)
        # 窗口最大化
        self.driver.maximize_window()
        self.driver.get("http://novel.hctestedu.com/")

    def tearDown(self) -> None:
        print("tearDown--后置执行--强制休眠、关闭浏览器等")
        sleep(3)
        self.driver.quit()

    @data("最强", "宠")
    # data数据：类似元组--一个个解析、传递给txt（唯一参数）
    def test_case1(self, txt):
        search_input = self.driver.find_element(By.ID, "searchKey")
        search_input.send_keys(txt)
        self.driver.find_element(By.ID, "btnSearch").click()
        print("执行用例test_case1")

    # 如果有两个参数怎么办？几组参数，几个()；几个参数，每个()中就有几个元素；每组数据中的元素必须一致（与参数数量一致），否则报错
    # 即 ("江少", "最强")对应(txt1, txt2)；共三组数据，意味着test_case2要执行三遍
    @data(("1江少", "最强"), ("2总", "少爷"), ("3宠", "凑数"))
    # data数据：类似元组--一个个解析、传递给txt（唯一参数） 需要解包？可以使用*txt1,**txt2吗？
    # 不行--意义不同，是给的过程中解包【@data(("江少", "最强", "宠"), ("总", "少爷"))】，
    # 而不是在接收时解包【test_case2(self, txt1, txt2):】 无法使用这种方式解包，要怎么办呢？
    # 使用装饰器 @ddt.unpack --- 用来解包data内的值的
    @unpack  # 则上方传入元组或列表均可
    def test_case2(self, txt1, txt2):
        print("执行用例test_case2")
        print(txt1)
        print(txt2)
        pass


"""    
测试结果：
    ============================= test session starts =============================
    collecting ... collected 5 items
    
    Test03_ddt_demo.py::Test03::test_case1_1_最强 
    Test03_ddt_demo.py::Test03::test_case1_2_宠 
    Test03_ddt_demo.py::Test03::test_case2_1___1江少____最强__ 
    Test03_ddt_demo.py::Test03::test_case2_2___2总____少爷__ 
    Test03_ddt_demo.py::Test03::test_case2_3___3宠____凑数__ 
    
    ============================= 5 passed in 59.39s ==============================
    
    Process finished with exit code 0
    PASSED                     [ 20%]setUp--前置执行--加载驱动，打开浏览器等
    执行用例test_case1
    tearDown--后置执行--强制休眠、关闭浏览器等
    PASSED                       [ 40%]setUp--前置执行--加载驱动，打开浏览器等
    执行用例test_case1
    tearDown--后置执行--强制休眠、关闭浏览器等
    PASSED         [ 60%]setUp--前置执行--加载驱动，打开浏览器等
    执行用例test_case2
    1江少
    最强
    tearDown--后置执行--强制休眠、关闭浏览器等
    PASSED           [ 80%]setUp--前置执行--加载驱动，打开浏览器等
    执行用例test_case2
    2总
    少爷
    tearDown--后置执行--强制休眠、关闭浏览器等
    PASSED           [100%]setUp--前置执行--加载驱动，打开浏览器等
    执行用例test_case2
    3宠
    凑数
    tearDown--后置执行--强制休眠、关闭浏览器等

"""

if __name__ == '__main__':
    unittest.main()
