""""""
"""
什么是unitTest（pyUnit）类似于 JUnit
--- 单元测试框架，可以结合selenium及request等

pytest>Unittest>RobotFramework

unittest不用搭建任何环境（python2.7.5后自带）

unittest 四个重要概念，四大组件
    Test fixture ： 
        相当于一个插件，对一个测试用例环境的待见和销毁，(提供两个方法)通过覆盖TestCase的setUp()和tearDown()方法来实现
        setUp()前置执行
        tearDown()后置执行
    Test case ： 
        一个TestCase就是一个测试用例。提供了断言机制（assertEqual等）
    Test suite ： 
        （多个用例组合执行，相当于列表）多个测试用例集合在一起，就是TestSuite，而且TestSuite也可以嵌套TestSuite。
    Test runner ： 
        是来执行测试用例的，其中的run(test)会执行TestSuite/TestCase中的run(result)方法。

"""
