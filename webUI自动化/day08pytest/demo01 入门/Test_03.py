import pytest


# pytest直接全部实例化，
class Test03Class:
    def test_one(self):
        print("------------dayin-----------")
        string = "王林测试"
        assert "测" in string

    def test_two(self):
        x = "王林测试"
        assert hasattr(x, 'ce')

    @pytest.mark.slow
    def test_three(self):
        x = "pytest.mark.slow"
        assert "slow" in x

    def test_four(self):
        x = "pytest.mark.slow"
        y = "slow"
        assert y in x


"""
系统设置可以设置执行方式 file--settings--Tools---Python Integrated Tools
Autodetect (pytest)
pytest
Nosetests
Unittests
Twisted Trial

unittest也可以通过pytest运行
"""
if __name__ == '__main__':
    pytest.main(['-vs', 'Test_03'])
