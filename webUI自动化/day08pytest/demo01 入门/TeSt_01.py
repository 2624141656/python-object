def add(x):
    return x + 2


# 命名规则不符合时 👇
"""
    PS E:\0arrange\test-code\python-object\webUI自动化\day08pytest\demo01 入门> pytest
    ==================================================== test session starts ===================================================== 
    platform win32 -- Python 3.9.7, pytest-7.2.1, pluggy-1.0.0
    rootdir: E:\0arrange\test-code\python-object\webUI自动化\day08pytest\demo01 入门
    collected 0 items                                                                                                              
    
    =================================================== no tests ran in 0.01s ==================================================== 

"""


def test_add():
    assert add(2) == 6
