import pytest


# pytest直接全部实例化，
class TestClass:
    def test_one(self):
        string = "王林测试"
        assert "测" in string

    def test_two(self):
        x = "王林测试"
        assert hasattr(x, 'ce')

    @pytest.mark.slow
    def test_three(self):
        x = "pytest.mark.slow"
        assert "slow" in x

    def test_four(self):
        x = "pytest.mark.slow"
        assert "slow" in x
