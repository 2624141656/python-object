""""""

"""
pytest 可以生成对应的测试报告 -- 利用插件 pytest-html
pytest-html
    安装： 
        $ pip install pytest-html
    执行生成报告：
        命令行： $ pytest -vs --html=(路径)/(报告名称).html
        打开 cmd cd 到需要执行的pytest用例的目录，执行命令
        $ pytest -vs --html=report.html 没有指定位置的话就会生成在当前路径 下
        指定位置：
        $ pytest -vs Test_10_html.py --html=./html-report/report.html
        
pycharm：TODO 框架封装 部分说

"""
