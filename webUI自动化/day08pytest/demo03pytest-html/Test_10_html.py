import pytest


@pytest.fixture(scope="module")
def open_test():
    print("打开浏览器")

    yield
    print("执行teardown操作")
    print("关闭浏览器")


def test_01(open_test):
    print("用例：搜索搜索搜索搜索111111")


def test_02(open_test):
    print("用例：搜索搜索搜索搜索222222")
    assert 1 > 2


def test_03(open_test):
    print("用例：搜索搜索搜索搜索333333")
