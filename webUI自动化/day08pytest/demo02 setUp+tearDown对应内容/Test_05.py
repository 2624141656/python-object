""""""

"""
pytest里 也包含了setUp和teardown对应内容，更细更全
用例运行级别
    模块级(setup_module/teardown_module):开始于模块的始末，全局的
    函数级(setup_function/teardown_function):只对函数用例生效（不在类中)
    类级(setup_class/teardown_class):只在类中前后运行一次
    方法级(setup_method/teardown_method):开始于方法始末
    类里面的(setup/teardown):运行在调用方法的前后
"""
import pytest


def setup_module():
    print("\n======模块级(setup_module):开始于模块的始末，全局的")


def teardown_module():
    print("\n======模块级(teardown_module):开始于模块的始末，全局的")


def setup_function():
    print("\n=========函数级(setup_function):只对函数用例生效（不在类中)")


def teardown_function():
    print("\n=========函数级(teardown_function):只对函数用例生效（不在类中)")


def test_func():
    print("\n第0个用例--执行")
    x = "pytest.mark.slow"
    y = "slow"
    assert y in x


class TestCase05:

    @staticmethod
    def setup():
        print("\n===类里面的(setup):运行在调用方法的前后")

    @staticmethod
    def teardown():
        print("\n===类里面的(teardown):运行在调用方法的前后")

    @staticmethod
    def setup_class():
        print("\n============类级(setup_class):只在类中前后运行一次")

    @staticmethod
    def teardown_class():
        print("\n============类级(teardown_class):只在类中前后运行一次")

    @staticmethod
    def setup_method():
        print("\n===============方法级(setup_method):开始于方法始末")

    @staticmethod
    def teardown_method():
        print("\n===============方法级(teardown_method):开始于方法始末")

    def test_one(self):
        print("\n第1个用例--执行")
        string = "王林测试"
        s = "测试"
        assert s in string

    @pytest.mark.slow
    def test_two(self):
        print("\n第2个用例--执行")
        x = "pytest.mark.slow"
        assert "slow" in x

    def test_three(self):
        print("\n第3个用例--执行")
        test_func()


if __name__ == '__main__':
    pytest.main(['-vs'])

"""
但是每次写一个，太傻了。通过pytest的fixture机制解决
"""
