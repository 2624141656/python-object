""""""

"""
pytest里 也包含了setUp和teardown对应内容，更细更全
用例运行级别
    模块级(setup_module/teardown_module):开始于模块的始末，全局的
    函数级(setup_function/teardown_function):只对函数用例生效（不在类中)
    类级(setup_class/teardown_class):只在类中前后运行一次
    方法级(setup_method/teardown_method):开始于方法始末
    类里面的(setup/teardown):运行在调用方法的前后
"""
import pytest


def setup_function():
    print("每个用例前都会执行我一次")


def teardown_function():
    print("每个用例后都会执行我一次")


def test_one():
    print("第一个用例--执行")
    string = "王林测试"
    s = "测试"
    assert s in string


def test_two():
    x = "王林测试"
    assert hasattr(x, 'ce')


@pytest.mark.slow
def test_three():
    x = "pytest.mark.slow"
    assert "slow" in x


def test_four():
    x = "pytest.mark.slow"
    y = "slow"
    assert y in x


if __name__ == '__main__':
    pytest.main(['-vs'])
