import pytest


@pytest.fixture(scope="module")
def open_test():
    print("打开浏览器")

    yield
    print("执行teardown操作")
    print("关闭浏览器")


def test_01(open_test):
    print("用例：搜索搜索搜索搜索111111")


"""
用例中遇到异常怎么办？
yield 下的代码还是会执行
    -- 执行测试用例 和 fixture修饰的函数 是连两个不同线程
但如果在fixture方法中异常，就无法通行 
"""


def test_02(open_test):
    print("用例：搜索搜索搜索搜索222222")
    assert 1 > 2
