"""
但是每次写一个setup等，太傻了。通过pytest的fixture机制解决

setUp 和 teardown 可以在前后加入一些操作，但是是全局生效的，如果要实现1需要执行前后肢，2不需要，3需要，怎么处理？
fixture机制：
    fixture(arg):
        arg : scope 参数有四个级别，function 默认级，class module session
        arg : params 可选参数列表，多个参数调用fixture功能，同时支持所有测试
        arg : autouse 可激活型
        arg : ids 每个字符串id列表
        arg : name fixture的名字
    返回值不是 return 而是 yield
"""
import pytest


# 不填则默认在所有函数中生效
@pytest.fixture()
def login():
    print("登录成功，账号是wanglin")


def test_1(login):
    print("test1，需要登录后才能执行")


def test_2():
    print("test2，不需要登录，就能执行")


def test_3(login):
    print("test3，需要登录，才能执行")


if __name__ == '__main__':
    pytest.main(['-vs'])

"""依旧不够智能 提出来 有个强制固定的文件名：conftest.py"""
