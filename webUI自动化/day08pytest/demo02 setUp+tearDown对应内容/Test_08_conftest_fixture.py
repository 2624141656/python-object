import pytest
from selenium import webdriver
import time


def test_01(global_driver):
    global_driver.get("http://novel.hctestedu.com/")
    time.sleep(2)
    title = global_driver.title
    assert "读书" in title


def test_02(global_driver, login):
    global_driver.get("http://novel.hctestedu.com/")
    time.sleep(2)
    title = global_driver.title
    assert "读书" in title
