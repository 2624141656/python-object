"""
全局共用 -- conftest.py
conftest特点：（全局）

1、可以跨.py文件调用 --全局共享的东西 --对象



"""

import pytest
import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service


@pytest.fixture()
def login():
    print("登录成功，账号是root")


"""
fixture 参数：
1、SCOPE 定义生效的级别（作用范围） setup/tearDown function（函数级别）
函数级：scope = function 每一个函数或者方法调用
类级别：class 每个测试类只执行一次
模块级：module 每个.py文件只执行一次
会话级：session 每次会话只运行一次--会话内的所有方法（函数）或者类，都在共享
        程序没结束，就默认为一次会话。程序重启，则生成一次新的会话。

fixture可以通过scope 控制 setup级别
既然有setup -- 用例执行之前的操作
就有 teardown -- 用例执行之后的操作
fixture teardown不是独立函数
yield 关键字，模拟 teardown操作
"""

# 需要在最外层定义，若无定义，则下方函数结果无法返回，导致undefined
driver = None


@pytest.fixture(scope='session', autouse=True)
def global_driver(request):
    global driver
    # 设定浏览器类型
    driver_path = os.path.abspath("../Utils") + "/chromedriver.exe"
    # 因为做了driver的定义，所以每次操作都会有浏览器一闪而过 -- 暂时无视
    if driver is None:
        option = webdriver.ChromeOptions()
        option.add_experimental_option("detach", True)
        driver_path_service = Service(driver_path)
        driver = webdriver.Chrome(service=driver_path_service, options=option)
        # 窗口最大化
        driver.maximize_window()
    driver.implicitly_wait(15)

    def end():
        driver.quit()

    # TODO
    request.addfinalizer(end)
    return driver
