import pytest


# 此处的login不需要引包，就会直接引用conftest中的方法
def test_1(login):
    print("test1，需要登录后才能执行")


def test_2():
    print("test2，不需要登录，就能执行")


def test_3(login):
    print("test3，需要登录，才能执行")


if __name__ == '__main__':
    pytest.main(['-vs'])
