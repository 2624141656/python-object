""""""

"""
pytest 
    python中的一种单元测试框架
优点：
    易上手，入门简单
    支持简单测试(单元测试)
    支持复杂测试(功能测试)
    丰富的参数且支持参数化
    跳过执行过程，标记测试用例
    支持重复执行失败用例
    完美兼容unittest
    自动生成测试报告
    集成jenkins
入门：
    安装 pytest 最新版本
        $ pip install -U pytest
    测试安装情况 查看版本 
        $ pip show pytest
        $ pytest --version
    pytest 命名规则
        在cmd(或IDEA的Terminal)中通过命令 pytest 即可执行运行，
        流程：查找当前目录 及 子目录 下，以test_*开头的文件，或者以 *_test 结尾的文件。然后找到这些文件中以 test_* 开头的方法
            条件缺一不可（文件大小写不敏感，可以任意大小写；方法必须小写test_*）
    多个测试用例
        指定文件名来运行对应的文件
            指定参数 -q ：显示简单结果
            $ pytest -q Test_02.py
    pytest用例规则
        1、文件名必须以test_*开头，或者以 *_test 结尾（文件大小写不敏感，可以任意大小写；）
            类名必须以Test*开头（大小写敏感，必须驼峰Test*）
        2、函数，都以 test_* 开头（方法必须小写test_*）
        3、test开头的类，不能带有__init__方法 【！！！】
            pytest已经直接全部实例化了（已用init方法），如果再写init方法，则直接报错
        4、使用pytest的那个文件夹，在包内，必须要有__init__.py
        5、断言 assert
    命令行参数
        -s：表示输出调试信息，包括print；
        -v：表示详细信息；可以同时使用 -vs
        -num：允许失败次数
        python -m：通过python启动用例。eg：python -m pytest -vs ./Test_02.py
        -k：通过关键字匹配文件名（则不需要输入全名）。eg：pytest -k "02"
        按节点匹配：只执行某个文件夹中的某个方法，通过 【文件名::类名】或【文件名::类名::方法名】 的方式匹配。
            eg：pytest Test_02.py::TestClass::test_one
        按装饰器--标记谋个内容的运行或者跳过：@pytest.mark.slow；通过 pytest -m 标签名
            eg：pytest -m slow
        -x：遇到错误就停止。 stopping after 1 failures
            eg：pytest -x Test_02.py
        --maxfail=num：用例错误达到num个数后，则停止测试
            eg：pytest --maxfail=1 Test_02.py    
"""

"""
Test_06
    fixture机制
        作用和setup和teardown一样
        命名灵活，叫什么名字均可
        conftest 配置中可实现数据共享，不要import
        可以实现多个py跨文件共享

"""
