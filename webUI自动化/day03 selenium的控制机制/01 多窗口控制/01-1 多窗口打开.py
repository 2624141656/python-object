""""""

"""
在学习day02 03 03-1时，学习查找页面多个元素，用百度热搜自练习，发现了，已自学会。。。copy过来，看视频是否有新知识

selenium在点击某个链接的时候，有时候会生成一个tab页，也就是窗口
此时要进行多窗口切换处理
句柄：每个窗口都有一个属性，handle标识各个窗口 -- 意味着程序的聚焦点在哪个窗口
switch_to.window 切换句柄
current_window_handle 当前窗口句柄
"""
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 关闭页签快捷键
from selenium.webdriver import Keys
from selenium.webdriver import ActionChains

# 0、加载驱动
driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
# 1、打开浏览器 创建chrom的实例对象
driver = webdriver.Chrome(service=driver_path_service, options=option)
# 2、页面最大化，输入网址。包括后期要使用ActionChains也需要页面最大化，否则快捷键操作可能会失败
driver.maximize_window()
driver.get("https://top.baidu.com/board?tab=realtime")
print(driver.title)
assert driver.title == "百度热搜"
print("跳转百度热搜页面测试通过")

try:
    print("跳转百度热搜 --- 打开新的页签")
    # 访问测试百度热搜的一个列表
    # 1、取到要测的整个list -- // 底下的所有a标签
    hot_lst = driver.find_elements(By.XPATH, '//*[@id="sanRoot"]/main/div[2]/div/div[2]/div/a')
    print("热搜有{}条".format(len(hot_lst)))
    # 2、循环遍历hot_lst
    for i, hots in enumerate(hot_lst):
        print(i)
        print(hots)
        hots.click()
        # 获取全部页面句柄
        all_handles = driver.window_handles
        # 移动句柄，对打开的新的页面进行操作
        driver.switch_to.window(all_handles[1])
        WebDriverWait(driver, 5).until(EC.number_of_windows_to_be(2), "等到切换到最新的页签")
        assert driver.title != "百度热搜"
        print("跳转热搜新闻【{}】详情测试通过".format(driver.title))
        cur_handle = driver.current_window_handle
        # 将当前句柄定位到新打开的页面
        driver.switch_to.window(cur_handle)
        # 关闭当前窗口。handle仍是cur_handle
        driver.close()
        # handle仍是cur_handle，跳转源点击页的handle，才能继续操作循环点击，否则error
        driver.switch_to.window(all_handles[0])
        WebDriverWait(driver, 5).until(EC.number_of_windows_to_be(1), "等到切换到最新的页签")
        # 一个是0一个是1是应为，一个是个数，一个是序号
# 学习使用，正常场景中，详情页测试覆盖了对应标签的测试
except Exception as e:
    print("打开新的页签 出错了，错误是：{}".format(e))
finally:
    # driver.close()
    print("打开新的页签 顺序输出，下方用例仍要访问，暂不退出浏览器")

sleep(5)
driver.quit()
