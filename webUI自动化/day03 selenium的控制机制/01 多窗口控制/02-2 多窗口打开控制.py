""""""

"""
1、尝试打开多个tab页窗口
2、tab间来回切换
如果不准备句柄，接下来我们来猜猜结果

"""
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 关闭页签快捷键
from selenium.webdriver import Keys
from selenium.webdriver import ActionChains

# 0、加载驱动
driver_path = r"D:\chromedriver\chromedriver-win64_116\chromedriver.exe"
driver_path_service = Service(driver_path)
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)
# 1、打开浏览器 创建chrom的实例对象
driver = webdriver.Chrome(service=driver_path_service, options=option)
# 2、页面最大化，输入网址。包括后期要使用ActionChains也需要页面最大化，否则快捷键操作可能会失败
driver.maximize_window()
driver.get("https://www.baidu.com/")
driver.implicitly_wait(30)
assert driver.title == "百度一下，你就知道"
print("获取百度搜索页面测试通过")

try:
    print("点击热搜新闻 --- 打开新的页签")
    a_links = driver.find_elements(By.XPATH, '//*[@id="hotsearch-content-wrapper"]/li/a')
    print("热搜有{}条".format(len(a_links)))
    for i in range(len(a_links)):
        print(i)
        a_links[i].click()
        all_handles = driver.window_handles
        driver.switch_to.window(all_handles[i + 1])
        # 因为先进先出问题，可以使用其他方式判断，比如input框要有title的文字
        WebDriverWait(driver, 5).until(EC.number_of_windows_to_be(i + 2), "等到切换到最新的页签再判断")
        assert driver.title != "百度一下，你就知道"
        print("跳转热搜新闻【{}】详情测试通过".format(driver.title))
        window_handle = driver.current_window_handle
        # driver.switch_to.window(window_handle)
        # driver.close()
        driver.switch_to.window(all_handles[0])
        """
        遵循栈的原则，先进后出
        即【百度一下，你就知道】是tab 0，后继所有新点开的页签都是 tab 1。先点开页签的先+1
        """

except Exception as e:
    print("打开新的页签 出错了，错误是：{}".format(e))
finally:
    # driver.close()
    print("打开新的页签 顺序输出，下方用例仍要访问，暂不退出浏览器")

sleep(5)
driver.quit()
